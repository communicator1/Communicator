package com.example.communicatorapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ImageView imageSlika;
    TextView textIme, textAdresa, textTelefon, textPol;
    Button button_main_1, button_main_2, button_main_3, button_main_4,
            button_act1_1, button_act1_2, button_act1_3, button_act1_4, button_act1_5, button_act1_6,
            button_act2_1, button_act2_2, button_act2_3, button_act2_4, button_act2_5, button_act2_6, button_act2_7, button_act2_8, button_act2_9,
            button_act3_1, button_act3_2, button_act3_3, button_act3_4, button_act3_5, button_act3_6, button_act3_7, button_act3_8,
            button_act4_1, button_act4_2, button_act4_3, button_act4_4, button_act4_5, button_act4_6, button_act4_7, button_act4_8;
    LinearLayout act_main, activity_1, activity_2, activity_2_1, activity_2_2, activity_3, activity_4;
    NavigationView navigationView;
    View headerView;
    MediaPlayer m_da, z_da, m_ne, z_ne, m_zdravo, z_zdravo, m_cao, z_cao, m_moze, z_moze, m_dojdi, z_dojdi,
            m_sakam, z_sakam, m_dajadam, z_dajadam, m_dapijam, z_dapijam, m_daigram, z_daigram, m_daodmoram, z_daodmoram,
            m_daspijam, z_daspijam, m_dasetam, z_dasetam, m_dacrtam, z_dacrtam, m_dacitam, z_dacitam,
            m_dagledamtelevizija, z_dagledamtelevizija, m_secuvstvuvam, z_secuvstvuvam, m_ubavo, z_ubavo,
            m_tazno, z_tazno, m_srekno, z_srekno, m_bolno, z_bolno, m_zalubeno, z_zalubeno, m_umorno, z_umorno,
            m_isplaseno, z_isplaseno, m_nervozno, z_nervozno, m_meboli, z_meboli, m_glava, z_glava, m_oko, z_oko,
            m_uvo, z_uvo, m_zab, z_zab, m_stomak, z_stomak, m_grb, z_grb, m_raka, z_raka, m_noga, z_noga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);


        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        initComponents();
        setComponents();

    }

    public void initComponents(){
        act_main = findViewById(R.id.layout_main);
        activity_1 = findViewById(R.id.layout_1);
        activity_2 = findViewById(R.id.layout_2);
        activity_2_1 = findViewById(R.id.layout_2_1);
        activity_2_2 = findViewById(R.id.layout_2_2);
        activity_3 = findViewById(R.id.layout_3);
        activity_4 = findViewById(R.id.layout_4);
        button_main_1 = findViewById(R.id.button_main_1);
        button_main_2 = findViewById(R.id.button_main_2);
        button_main_3 = findViewById(R.id.button_main_3);
        button_main_4 = findViewById(R.id.button_main_4);
        button_act1_1 = findViewById(R.id.button_activity1_1);
        button_act1_2 = findViewById(R.id.button_activity1_2);
        button_act1_3 = findViewById(R.id.button_activity1_3);
        button_act1_4 = findViewById(R.id.button_activity1_4);
        button_act1_5 = findViewById(R.id.button_activity1_5);
        button_act1_6 = findViewById(R.id.button_activity1_6);
        button_act2_1 = findViewById(R.id.button_activity2_1);
        button_act2_2 = findViewById(R.id.button_activity2_2);
        button_act2_3 = findViewById(R.id.button_activity2_3);
        button_act2_4 = findViewById(R.id.button_activity2_4);
        button_act2_5 = findViewById(R.id.button_activity2_5);
        button_act2_6 = findViewById(R.id.button_activity2_6);
        button_act2_7 = findViewById(R.id.button_activity2_7);
        button_act2_8 = findViewById(R.id.button_activity2_8);
        button_act2_9 = findViewById(R.id.button_activity2_9);
        button_act3_1 = findViewById(R.id.button_activity3_1);
        button_act3_2 = findViewById(R.id.button_activity3_2);
        button_act3_3 = findViewById(R.id.button_activity3_3);
        button_act3_4 = findViewById(R.id.button_activity3_4);
        button_act3_5 = findViewById(R.id.button_activity3_5);
        button_act3_6 = findViewById(R.id.button_activity3_6);
        button_act3_7 = findViewById(R.id.button_activity3_7);
        button_act3_8 = findViewById(R.id.button_activity3_8);
        button_act4_1 = findViewById(R.id.button_activity4_1);
        button_act4_2 = findViewById(R.id.button_activity4_2);
        button_act4_3 = findViewById(R.id.button_activity4_3);
        button_act4_4 = findViewById(R.id.button_activity4_4);
        button_act4_5 = findViewById(R.id.button_activity4_5);
        button_act4_6 = findViewById(R.id.button_activity4_6);
        button_act4_7 = findViewById(R.id.button_activity4_7);
        button_act4_8 = findViewById(R.id.button_activity4_8);

        navigationView = findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);

        imageSlika = headerView.findViewById(R.id.Slika);
        textIme = headerView.findViewById(R.id.Ime);
        textAdresa = headerView.findViewById(R.id.Adresa);
        textTelefon = headerView.findViewById(R.id.Telefon);
        textPol = headerView.findViewById(R.id.Pol);

        m_da = MediaPlayer.create(this, R.raw.m_da);
        z_da = MediaPlayer.create(this, R.raw.z_da);
        m_ne = MediaPlayer.create(this, R.raw.m_ne);
        z_ne = MediaPlayer.create(this, R.raw.z_ne);
        m_zdravo = MediaPlayer.create(this, R.raw.m_zdravo);
        z_zdravo = MediaPlayer.create(this, R.raw.z_zdravo);
        m_cao = MediaPlayer.create(this, R.raw.m_cao);
        z_cao = MediaPlayer.create(this, R.raw.z_cao);
        m_moze = MediaPlayer.create(this, R.raw.m_moze);
        z_moze = MediaPlayer.create(this, R.raw.z_moze);
        m_dojdi = MediaPlayer.create(this, R.raw.m_dojdi);
        z_dojdi = MediaPlayer.create(this, R.raw.z_dojdi);
        m_sakam = MediaPlayer.create(this, R.raw.m_sakam);
        z_sakam = MediaPlayer.create(this, R.raw.z_sakam);
        m_dajadam = MediaPlayer.create(this, R.raw.m_dajadam);
        z_dajadam = MediaPlayer.create(this, R.raw.z_dajadam);
        m_dapijam = MediaPlayer.create(this, R.raw.m_dapijam);
        z_dapijam = MediaPlayer.create(this, R.raw.z_dapijam);
        m_daigram = MediaPlayer.create(this, R.raw.m_daigram);
        z_daigram = MediaPlayer.create(this, R.raw.z_daigram);
        m_daodmoram = MediaPlayer.create(this, R.raw.m_daodmoram);
        z_daodmoram = MediaPlayer.create(this, R.raw.z_daodmoram);
        m_daspijam = MediaPlayer.create(this, R.raw.m_daspijam);
        z_daspijam = MediaPlayer.create(this, R.raw.z_daspijam);
        m_dasetam = MediaPlayer.create(this, R.raw.m_dasetam);
        z_dasetam = MediaPlayer.create(this, R.raw.z_dasetam);
        m_dacrtam = MediaPlayer.create(this, R.raw.m_dacrtam);
        z_dacrtam = MediaPlayer.create(this, R.raw.z_dacrtam);
        m_dacitam = MediaPlayer.create(this, R.raw.m_dacitam);
        z_dacitam = MediaPlayer.create(this, R.raw.z_dacitam);
        m_dagledamtelevizija = MediaPlayer.create(this, R.raw.m_dagledamtelevizija);
        z_dagledamtelevizija = MediaPlayer.create(this, R.raw.z_dagledamtelevizija);
        m_secuvstvuvam = MediaPlayer.create(this, R.raw.m_secuvstvuvam);
        z_secuvstvuvam = MediaPlayer.create(this, R.raw.z_secuvstvuvam);
        m_ubavo = MediaPlayer.create(this, R.raw.m_ubavo);
        z_ubavo = MediaPlayer.create(this, R.raw.z_ubavo);
        m_tazno = MediaPlayer.create(this, R.raw.m_tazno);
        z_tazno = MediaPlayer.create(this, R.raw.z_tazno);
        m_srekno = MediaPlayer.create(this, R.raw.m_srekno);
        z_srekno = MediaPlayer.create(this, R.raw.z_srekno);
        m_bolno = MediaPlayer.create(this, R.raw.m_bolno);
        z_bolno = MediaPlayer.create(this, R.raw.z_bolno);
        m_zalubeno = MediaPlayer.create(this, R.raw.m_zalubeno);
        z_zalubeno = MediaPlayer.create(this, R.raw.z_zalubeno);
        m_umorno = MediaPlayer.create(this, R.raw.m_umorno);
        z_umorno = MediaPlayer.create(this, R.raw.z_umorno);
        m_isplaseno = MediaPlayer.create(this, R.raw.m_isplaseno);
        z_isplaseno = MediaPlayer.create(this, R.raw.z_isplaseno);
        m_nervozno = MediaPlayer.create(this, R.raw.m_nervozno);
        z_nervozno = MediaPlayer.create(this, R.raw.z_nervozno);
        m_meboli = MediaPlayer.create(this, R.raw.m_meboli);
        z_meboli = MediaPlayer.create(this, R.raw.z_meboli);
        m_glava = MediaPlayer.create(this, R.raw.m_glava);
        z_glava = MediaPlayer.create(this, R.raw.z_glava);
        m_oko = MediaPlayer.create(this, R.raw.m_oko);
        z_oko = MediaPlayer.create(this, R.raw.z_oko);
        m_uvo = MediaPlayer.create(this, R.raw.m_uvo);
        z_uvo = MediaPlayer.create(this, R.raw.z_uvo);
        m_zab = MediaPlayer.create(this, R.raw.m_zab);
        z_zab = MediaPlayer.create(this, R.raw.z_zab);
        m_stomak = MediaPlayer.create(this, R.raw.m_stomak);
        z_stomak = MediaPlayer.create(this, R.raw.z_stomak);
        m_grb = MediaPlayer.create(this, R.raw.m_grb);
        z_grb = MediaPlayer.create(this, R.raw.z_grb);
        m_raka = MediaPlayer.create(this, R.raw.m_raka);
        z_raka = MediaPlayer.create(this, R.raw.z_raka);
        m_noga = MediaPlayer.create(this, R.raw.m_noga);
        z_noga = MediaPlayer.create(this, R.raw.z_noga);
    }

    public void setComponents(){
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        SharedPreferences sp1=this.getSharedPreferences("ActivityPREF", MODE_PRIVATE);
        String ime = sp1.getString("ime", null);
        String adresa = sp1.getString("adresa", null);
        String telefon = sp1.getString("telefon", null);
        String encodedSlika = sp1.getString("slika", null);
        String pol = sp1.getString("pol", "Машко");
        if(encodedSlika!=null){
            byte[] b = Base64.decode(encodedSlika, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            imageSlika.setImageBitmap(bitmap);
        }
        textIme.setText(ime);
        textAdresa.setText(adresa);
        textTelefon.setText(telefon);
        textPol.setText(pol);

        activity_2.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeLeft(){
                activity_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
        });
        button_act2_1.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeLeft(){
                activity_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_dajadam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_dajadam.start();
                }
            }
        });
        button_act2_2.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeLeft(){
                activity_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_dapijam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_dapijam.start();
                }
            }
        });
        button_act2_3.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeLeft(){
                activity_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_daigram.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_daigram.start();
                }
            }
        });

        activity_2_1.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2.setVisibility(View.VISIBLE);
            }
            public void onSwipeLeft(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2_2.setVisibility(View.VISIBLE);
            }
        });
        button_act2_4.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2.setVisibility(View.VISIBLE);
            }
            public void onSwipeLeft(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2_2.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_daodmoram.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_daodmoram.start();
                }
            }
        });
        button_act2_5.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2.setVisibility(View.VISIBLE);
            }
            public void onSwipeLeft(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2_2.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_daspijam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_daspijam.start();
                }
            }
        });
        button_act2_6.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2.setVisibility(View.VISIBLE);
            }
            public void onSwipeLeft(){
                activity_2_1.setVisibility(View.INVISIBLE);
                activity_2_2.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_dasetam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_dasetam.start();
                }
            }
        });

        activity_2_2.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
        });
        button_act2_7.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_dacrtam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_dacrtam.start();
                }
            }
        });
        button_act2_8.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_dacitam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_dacitam.start();
                }
            }
        });
        button_act2_9.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this){
            public void onSwipeRight(){
                activity_2_2.setVisibility(View.INVISIBLE);
                activity_2_1.setVisibility(View.VISIBLE);
            }
            @Override
            public void onClick() {
                if(textPol.getText().toString().equals("Машко")){
                    m_dagledamtelevizija.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_dagledamtelevizija.start();
                }
            }
        });

        button_main_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                act_main.setVisibility(View.INVISIBLE);
                activity_1.setVisibility(View.VISIBLE);
                navigationView.getMenu().getItem(1).setChecked(true);
            }
        });

        button_main_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View event) {
                act_main.setVisibility(View.INVISIBLE);
                activity_2.setVisibility(View.VISIBLE);
                navigationView.getMenu().getItem(2).setChecked(true);
                if(textPol.getText().toString().equals("Машко")){
                    m_sakam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_sakam.start();
                }
            }
        });
        button_main_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View event) {
                act_main.setVisibility(View.INVISIBLE);
                activity_3.setVisibility(View.VISIBLE);
                navigationView.getMenu().getItem(3).setChecked(true);
                if(textPol.getText().toString().equals("Машко")){
                    m_secuvstvuvam.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_secuvstvuvam.start();
                }
            }
        });
        button_main_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View event) {
                act_main.setVisibility(View.INVISIBLE);
                activity_4.setVisibility(View.VISIBLE);
                navigationView.getMenu().getItem(4).setChecked(true);
                if(textPol.getText().toString().equals("Машко")){
                    m_meboli.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_meboli.start();
                }
            }
        });

        button_act1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_da.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_da.start();
                }
            }
        });
        button_act1_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_ne.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_ne.start();
                }
            }
        });
        button_act1_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_zdravo.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_zdravo.start();
                }
            }
        });
        button_act1_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_cao.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_cao.start();
                }
            }
        });
        button_act1_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_moze.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_moze.start();
                }
            }
        });
        button_act1_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_dojdi.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_dojdi.start();
                }
            }
        });
        button_act3_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_ubavo.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_ubavo.start();
                }
            }
        });
        button_act3_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_tazno.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_tazno.start();
                }
            }
        });
        button_act3_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_srekno.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_srekno.start();
                }
            }
        });
        button_act3_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_bolno.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_bolno.start();
                }
            }
        });
        button_act3_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_zalubeno.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_zalubeno.start();
                }
            }
        });
        button_act3_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_umorno.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_umorno.start();
                }
            }
        });
        button_act3_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_isplaseno.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_isplaseno.start();
                }
            }
        });
        button_act3_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_nervozno.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_nervozno.start();
                }
            }
        });
        button_act4_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_glava.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_glava.start();
                }
            }
        });
        button_act4_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_oko.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_oko.start();
                }
            }
        });
        button_act4_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_uvo.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_uvo.start();
                }
            }
        });
        button_act4_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_zab.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_zab.start();
                }
            }
        });
        button_act4_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_stomak.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_stomak.start();
                }
            }
        });
        button_act4_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_grb.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_grb.start();
                }
            }
        });
        button_act4_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_raka.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_raka.start();
                }
            }
        });
        button_act4_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textPol.getText().toString().equals("Машко")){
                    m_noga.start();
                }
                else if(textPol.getText().toString().equals("Женско")){
                    z_noga.start();
                }
            }
        });
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(act_main.getVisibility()==View.INVISIBLE)
        {
            act_main.setVisibility(View.VISIBLE);
            activity_1.setVisibility(View.INVISIBLE);
            activity_2.setVisibility(View.INVISIBLE);
            activity_2_1.setVisibility(View.INVISIBLE);
            activity_2_2.setVisibility(View.INVISIBLE);
            activity_3.setVisibility(View.INVISIBLE);
            activity_4.setVisibility(View.INVISIBLE);
            navigationView.getMenu().getItem(0).setChecked(true);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_promeni) {
            Intent intent = new Intent(this, ChangeInfoActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.about_me) {
            new AlertDialog.Builder(MainActivity.this)
            .setMessage("Оваа апликација е направена од Ѓорѓи Смилевски, студент на ФЕИТ, за организацијата " +
                    "Отворете Ги Прозорците, во соработка со професорите Бранислав Геразов и Христијан Ѓорески.\n" +
                    "Контакт: smilevskigjorgji@gmail.com")
            .setNeutralButton("Назад", null)
            .show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_pocetna) {
            act_main.setVisibility(View.VISIBLE);
            activity_1.setVisibility(View.INVISIBLE);
            activity_2.setVisibility(View.INVISIBLE);
            activity_2_1.setVisibility(View.INVISIBLE);
            activity_2_2.setVisibility(View.INVISIBLE);
            activity_3.setVisibility(View.INVISIBLE);
            activity_4.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_socijalizacija) {
            act_main.setVisibility(View.INVISIBLE);
            activity_1.setVisibility(View.VISIBLE);
            activity_2.setVisibility(View.INVISIBLE);
            activity_2_1.setVisibility(View.INVISIBLE);
            activity_2_2.setVisibility(View.INVISIBLE);
            activity_3.setVisibility(View.INVISIBLE);
            activity_4.setVisibility(View.INVISIBLE);
        } else if (id == R.id.nav_sakam) {
            act_main.setVisibility(View.INVISIBLE);
            activity_1.setVisibility(View.INVISIBLE);
            activity_2.setVisibility(View.VISIBLE);
            activity_2_1.setVisibility(View.INVISIBLE);
            activity_2_2.setVisibility(View.INVISIBLE);
            activity_3.setVisibility(View.INVISIBLE);
            activity_4.setVisibility(View.INVISIBLE);
            if(textPol.getText().toString().equals("Машко")){
                m_sakam.start();
            }
            else if(textPol.getText().toString().equals("Женско")){
                z_sakam.start();
            }
        } else if (id == R.id.nav_cuvstvuvam) {
            act_main.setVisibility(View.INVISIBLE);
            activity_1.setVisibility(View.INVISIBLE);
            activity_2.setVisibility(View.INVISIBLE);
            activity_2_1.setVisibility(View.INVISIBLE);
            activity_2_2.setVisibility(View.INVISIBLE);
            activity_3.setVisibility(View.VISIBLE);
            activity_4.setVisibility(View.INVISIBLE);
            if(textPol.getText().toString().equals("Машко")){
                m_secuvstvuvam.start();
            }
            else if(textPol.getText().toString().equals("Женско")){
                z_secuvstvuvam.start();
            }
        } else if (id == R.id.nav_boli) {
            act_main.setVisibility(View.INVISIBLE);
            activity_1.setVisibility(View.INVISIBLE);
            activity_2.setVisibility(View.INVISIBLE);
            activity_2_1.setVisibility(View.INVISIBLE);
            activity_2_2.setVisibility(View.INVISIBLE);
            activity_3.setVisibility(View.INVISIBLE);
            activity_4.setVisibility(View.VISIBLE);
            if(textPol.getText().toString().equals("Машко")){
                m_meboli.start();
            }
            else if(textPol.getText().toString().equals("Женско")){
                z_meboli.start();
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
